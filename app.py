from flask import Flask, request, render_template, flash, redirect, url_for, session
from flaskext.mysql import MySQL
from wtforms import Form, StringField, TextAreaField, PasswordField, validators
from passlib.hash import sha256_crypt
from flask_sqlalchemy import SQLAlchemy
import datetime
from sqlalchemy import create_engine
from pytz import timezone
import alchemy
from functools import wraps
from sqlalchemy import Column, Integer, DateTime
from sqlalchemy.sql import func
from datetime import datetime
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import inspect



app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///D:\\PYTHON_COURSE\\flaskCRUd\\\\test.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False 

mysql = MySQL(app)
mysql.init_app(app)


def query_to_dict(ret):
    if ret is not None:
        return [{key: value for key, value in row.items()} for row in ret if row is not None]
    else:
       return None


@app.route('/')
def template():
    return render_template('home.html')


@app.route('/about')
def about():
    return render_template('about.html')


@app.route('/articles')
def articles():
    articles = alchemy.Articles.query.all()
    if article is not None:
        return render_template('articles.html', articles=articles)
    else:
        msg = 'no articles found'
        return render_template('articles.html', msg=msg)


@app.route('/article/<string:id>/')
def article(id):

    article = alchemy.Articles.query.get(id)
    return render_template('article.html', article=article)


class RegisterForm(Form):
    name = StringField('Name', [validators.Length(min=1, max=50)])
    username = StringField('Username', [validators.Length(min=4, max=25)])
    email = StringField('Email', [validators.Length(min=6, max=50)])
    password = PasswordField('Password', [
        validators.DataRequired(),
        validators.EqualTo('confirm', message='Password do not match!')
    ])
    confirm = PasswordField('Confirm password')


db = SQLAlchemy(app)
@app.route('/register', methods=['GET', 'POST'])
def register():
    form = RegisterForm(request.form)
    if request.method == 'POST' and form.validate():

        UTC = timezone('UTC')
        name = form.name.data
        email = form.email.data
        username = form.username.data
        password = sha256_crypt.encrypt(str(form.password.data))
        created_date = datetime.now(UTC)
        new = alchemy.User(name, username, email, password, created_date)
        db.session.add(new)
        db.session.commit()

        flash('You are now registered and can log in', 'success')

        return redirect(url_for('login'))
    return render_template('register.html', form=form)


@app.route('/login', methods=['GET', 'POST'])
def login():

    engine = create_engine('sqlite:///D:\\PYTHON_COURSE\\flaskCRUd\\\\test.db')
    if request.method == 'POST':
        username = request.form['username']
        password_candidate = request.form['password']

        with engine.connect() as con:

            result = con.execute(f"SELECT * FROM user WHERE username = '{username}'")
            data = query_to_dict(result)

            if len(data) > 0:

                password = data[0].get('password')
                if sha256_crypt.verify(password_candidate, password):
                    session['logged_in'] = True
                    session['username'] = username

                    flash('You are loged in', 'success')
                    return redirect(url_for('dashboard'))
                else:
                    error = 'INVALID LOGIN'
                    return render_template('login.html', error=error)
                con.close()

            else:
                error = 'USERNAME NOT FOUND'
                return render_template('login.html', error=error)

    return render_template('login.html')


def is_logged_in(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if 'logged_in' in session:
            return f(*args, **kwargs)
        else:
            flash('Unauthorized, Please login', 'danger')
            return redirect(url_for('login'))
    return wrap


@app.route('/logout')
@is_logged_in
def logout():
    session.clear()
    flash('You are logged out', 'success')
    return redirect(url_for('login'))


@app.route('/dashboard')
@is_logged_in
def dashboard():
    articles = alchemy.Articles.query.all()
    if article is not None:
        return render_template('dashboard.html', articles=articles)
    else:
        msg = 'no articles found'
        return render_template('dashboard.html', msg=msg)


class ArticleForm(Form):
    title = StringField('Title', [validators.Length(min=1, max=200)])
    body = TextAreaField('body', [validators.Length(min=10)])


@app.route('/add_article', methods=['Get', 'POST'])
@is_logged_in
def add_article():
    form = ArticleForm(request.form)
    if request.method == 'POST' and form.validate():

        UTC = timezone('UTC')
        title = form.title.data
        body = form.body.data
        name = session.get("username")
        created_date = datetime.now(UTC)

        new = alchemy.Articles(title, body, name, created_date)
        db.session.add(new)
        db.session.commit()

        flash('Article created', 'success')
        return redirect(url_for('dashboard'))

    return render_template('/add_article.html', form=form)


@app.route('/edit_article/<string:id>', methods=['GET', 'POST'])
@is_logged_in
def edit_article(id):

    article = db.session.query(alchemy.Articles).filter_by(id=id).first() 
    form = ArticleForm(request.form)

    form.title.data = article.title
    form.body.data = article.body

    if request.method == 'POST' and form.validate():
        article.title = request.form['title']
        article.body = request.form['body']
        db.session.commit()
        db.session.close()

        flash('Article Updated', 'success')

        return redirect(url_for('dashboard'))

    return render_template('edit_article.html', form=form)


@app.route('/delete_article/<string:id>', methods=['POST'])
@is_logged_in
def delete_article(id):
    article = db.session.query(alchemy.Articles).filter_by(id=id).first()
    db.session.delete(article)
    db.session.commit()

    flash('Article Deleted', 'success')

    return redirect(url_for('dashboard'))


if __name__ == '__main__':
    app.secret_key = 'secret'
    app.run(debug=True)

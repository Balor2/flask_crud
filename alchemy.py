import datetime
from sqlalchemy.sql import func
from sqlalchemy import Column, Integer, DateTime

from app import db


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), unique=True, nullable=False)
    email = db.Column(db.String(80), unique=True, nullable=False)
    username = db.Column(db.String(80), unique=True, nullable=False)
    password = db.Column(db.String(120), nullable=False)
    created_date = db.Column(DateTime(timezone=True), default=func.now())

    def __init__(self, name, username, email, password, created_date):
        
        self.name = name
        self.username = username
        self.email = email
        self.password = password
        self.created_date = created_date
    
    def get_passwd(self):
        return self.__password

    def get_name(self):
        return self.__name

    def get_email(self):
        return self.__email

    def get_username(self):
        return self.__username
    
    def get_create_date(self):
        return self.__create_date


class Articles(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(80), unique=False, nullable=False)
    body = db.Column(db.Text, unique=False, nullable=False)
    author = db.Column(db.String(80), unique=False, nullable=False)
    created_date = db.Column(DateTime(timezone=False), default=func.now())

    def __init__(self, title, body, author, created_date):
        
        self.title = title
        self.body = body
        self.author = author
        self.created_date = created_date
